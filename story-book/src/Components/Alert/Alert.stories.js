import React from "react";
import Alert from "./Alert";

export default {
    title: "Alert",
    component: Alert,
    tags: ["autodocs"]
};

export const Success = () => (
    <Alert variant="success" title="success" text="This is Alert for Success" />
);
export const Error = () => (
    <Alert variant="error" title="error" text="This is Alert for Error" />
);
