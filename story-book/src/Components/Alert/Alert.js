import React from "react";
import "./Alert.css";

const Alert = (props) => {
    const {
        variant = "primary",
        title = "Your Title",
        text = "Lorem ipsum dolor sit",
        ...rest
    } = props;
    return (
        <div className={`alert ${variant}`} {...rest}>
            <h4>{title}</h4>
            <p>{text}</p>
        </div>
    );
};

export default Alert;
