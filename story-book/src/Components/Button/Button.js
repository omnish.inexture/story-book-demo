import React from "react";
import "./Button.css";
import PropTypes from "prop-types";

const Button = (props) => {
    const { variant = "primary", children = "Click Me", ...rest } = props;
    return (
        <button className={`button ${variant}`} {...rest}>
            {children}
        </button>
    );
};

export default Button;

Button.prototype = {
    variant: PropTypes.oneOf(["primary", "secondary", "success", "danger"])
};
