/** @type { import('@storybook/react').Preview } */
import * as DocBlocks from "@storybook/blocks";
import React from "react";
const preview = {
    parameters: {
        docs: {
            autodocs: true,
            page: () => (
                <>
                    <DocBlocks.Title />
                    <DocBlocks.Primary />
                    <DocBlocks.Stories />
                </>
            )
        },
        actions: { argTypesRegex: "^on[A-Z].*" },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/
            }
        }
    }
};

export default preview;
