import "./App.css";
import { Button, Alert } from "storybook-demo-npm";

function App() {
    return (
        <>
            <div className="alert__container">
                <Alert title="success" variant="success" text="Success!!!" />
            </div>
            <div className="button__container">
                <Button variant="primary">Primary</Button>
                <Button variant="secondary">Secondary</Button>
                <Button variant="success">Success</Button>
                <Button variant="danger">Danger</Button>
            </div>
        </>
    );
}

export default App;
